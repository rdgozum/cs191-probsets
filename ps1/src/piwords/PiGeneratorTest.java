package piwords;

import static org.junit.Assert.*;

import org.junit.Test;

public class PiGeneratorTest {
   
    @Test
    public void basicPowerModTest() {
        assertEquals(17, PiGenerator.powerMod(5, 7, 23));
        assertEquals(6, PiGenerator.powerMod(6, 7, 7));
        assertEquals(4, PiGenerator.powerMod(4, 4, 9));
        assertEquals(1, PiGenerator.powerMod(12, 8, 13));
        assertEquals(8, PiGenerator.powerMod(8, 5, 28));
        assertEquals(0, PiGenerator.powerMod(0, 5, 2));
        //assertEquals(0, PiGenerator.powerMod(4, 5, 0));
    }
    
    @Test
    public void computePiInHexTest() {
        //[2, 4, 3, 15, 6, 10, 8, 8, 8, 5, 10, 
        int[] test1 = {2, 4, 3, 15, 6, 10, 8, 8, 8, 5};
        assertArrayEquals(test1, PiGenerator.computePiInHex(10));
        int[] test2 = {2, 4, 3, 15, 6, 10, 8};
        assertArrayEquals(test2, PiGenerator.computePiInHex(7));
        int[] test3 = {2, 4};
        assertArrayEquals(test3, PiGenerator.computePiInHex(2));
        
    }
  
    // TODO: Write more tests (Problem 1.a, 1.c)
}
