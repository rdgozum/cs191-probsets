package piwords;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

public class WordFinderTest {
    @Test
    public void basicGetSubstringsTest() {
        Map<String, Integer> expectedOutput = new HashMap<String, Integer>();
        
        String haystack = "abcde";
        String[] needles = {"ab", "abc", "de", "fg", "bcd"};

        expectedOutput.put("ab", 0);
        expectedOutput.put("abc", 0);
        expectedOutput.put("de", 3);
        expectedOutput.put("bcd", 1);

        assertEquals(expectedOutput, WordFinder.getSubstrings(haystack,
                                                              needles));
        
        //TEST1
        haystack = "ryanpaulgozum";
        String[] test_1 = {"paul", "ryan", "gozum", "npa", "pqr"};

        expectedOutput = new HashMap<String, Integer>();
        expectedOutput.put("paul", 4);
        expectedOutput.put("ryan", 0);
        expectedOutput.put("gozum", 8);
        expectedOutput.put("npa", 3);

        assertEquals(expectedOutput, WordFinder.getSubstrings(haystack,
                                                              test_1));
        
        //TEST2
        haystack = "computerscience";
        String[] test_2 = {"comp", "science", "sci", "put", "r", "cute"};

        expectedOutput = new HashMap<String, Integer>();
        expectedOutput.put("comp", 0);
        expectedOutput.put("science", 8);
        expectedOutput.put("sci", 8);
        expectedOutput.put("put", 3);
        expectedOutput.put("r", 7);

        assertEquals(expectedOutput, WordFinder.getSubstrings(haystack,
                                                              test_2));
        
        
    }

    // TODO: Write more tests (Problem 4.a)
}
