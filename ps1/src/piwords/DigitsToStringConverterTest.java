package piwords;

import static org.junit.Assert.*;

import org.junit.Test;

public class DigitsToStringConverterTest {
    @Test
    public void basicNumberSerializerTest() {
        // Input is a 4 digit number, 0.123 represented in base 4
        int[] input = {0, 1, 2, 3};
        // Want to map 0 -> "d", 1 -> "c", 2 -> "b", 3 -> "a"
        char[] alphabet = {'d', 'c', 'b', 'a'};

        String expectedOutput = "dcba";
        assertEquals(expectedOutput,DigitsToStringConverter.convertDigitsToString(input, 4, alphabet));
      
        //TEST1
        int[] test1 = {3, 4, 1, 2, 0};
        char[] letters1= {'a', 'g', 'e', 'd', 's'};
        
        String output1="dsgea";
        assertEquals(output1,DigitsToStringConverter.convertDigitsToString(test1, 5, letters1));
        //TEST2
        int[] test2 = {3, 4, 1, 2, 0, 0, 7};
        char[] letters2= {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'};
        
        String output2="debcaah";
        assertEquals(output2,DigitsToStringConverter.convertDigitsToString(test2, 8, letters2));
        //TEST3
        int[] test3 = {0, 4, 1, 2, 0, 3, 5};
        char[] letters3= {'a', 'b', 'c', 'd', 'e', 'f'};
        
        String output3="aebcadf";
        assertEquals(output3,DigitsToStringConverter.convertDigitsToString(test3, 6, letters3));
        
        
    }

    // TODO: Write more tests (Problem 3.a)
}
