package piwords;

import java.util.HashMap;
import java.util.Map;

public class WordFinder {
    /**
     * Given a String (the haystack) and an array of Strings (the needles),
     * return a Map<String, Integer>, where keys in the map correspond to
     * elements of needles that were found as substrings of haystack, and the
     * value for each key is the lowest index of haystack at which that needle
     * was found. A needle that was not found in the haystack should not be
     * returned in the output map.
     * 
     * @param haystack The string to search into.
     * @param needles The array of strings to search for. This array is not
     *                mutated.
     * @return The list of needles that were found in the haystack.
     */
    public static Map<String, Integer> getSubstrings(String haystack,
                                                     String[] needles) { 
        Map<String, Integer> expectedOutput = new HashMap<String, Integer>();
        
        int k, l;
        char haystack_char[]=haystack.toCharArray();
        for(int i=0; i<needles.length; i++) { //scanning the substrings array
            String temp=needles[i];
            for(int j=0; j<haystack.length(); j++) { //scanning the haystack.. abcdefghij    j=9
                if(haystack_char[j]==temp.charAt(0)) {
                    l=j;
                    for(k=0; k<temp.length(); k++) {
                        if(l==haystack.length()) break;
                        if(temp.charAt(k)!=haystack_char[l]) break;
                        l++;
                    }
                    if(k==temp.length()) {
                        expectedOutput.put(temp, j);
                    }
                }
            } //end of for
        } //end of for
        return expectedOutput;
               
    }
}
